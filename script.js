let currentSlide = 0;

function showSlide(index) {
    const carousel = document.getElementById('carousel');
    const slides = document.querySelectorAll('.carousel-item');

    if (index < 0) {
        currentSlide = slides.length - 1;
    } else if (index >= slides.length) {
        currentSlide = 0;
    } else {
        currentSlide = index;
    }

    const newTransformValue = -currentSlide * 100 + '%';
    carousel.style.transform = 'translateX(' + newTransformValue + ')';
}

function nextSlide() {
    showSlide(currentSlide + 1);
}

function prevSlide() {
    showSlide(currentSlide - 1);
}

// Auto-play carousel every 3 seconds
setInterval(nextSlide, 3000);
