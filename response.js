var token = document.getElementById("token");
var formTambah = document.getElementById("tambah");
var formUpdate = document.getElementById("update");

if (localStorage.getItem('key')) {
    token.style.display = "none";
    formUpdate.style.display = "none";
    fetch("https://crudcrud.com/api/" + localStorage.getItem('key') + "/data")
        .then((response) => response.json())
        .then((data) => {
            displayData(data);
        })
        .catch((error) => {
            console.error("Terjadi kesalahan:", error);
        });
} else {
    formTambah.style.display = "none";
    formUpdate.style.display = "none";
}

function displayData(data) {
    const tbody = document.getElementById("dataBody");
    tbody.removeChild(tbody.firstChild);
    var position = 0;
    data.forEach(function (item) {
        position++;
        const tr = document.createElement("tr");
        const tdNo = document.createElement("td");
        const tdId = document.createElement("td");
        const tdName = document.createElement("td");
        const tdKelas = document.createElement("td");
        const tdAksi = document.createElement("td");

        const buttonDelete = document.createElement("button");
        const buttonUbah = document.createElement("button");

        tdNo.textContent = position;
        tdId.textContent = item._id;
        tdName.textContent = item.name;
        tdKelas.textContent = item.kelas;

        buttonDelete.textContent = "Hapus";
        buttonDelete.classList.add("btn-danger");
        buttonDelete.addEventListener("click", function () {
            deleteData(item._id, item.name);
        });

        buttonUbah.textContent = "Ubah";
        buttonUbah.classList.add("btn-info");
        buttonUbah.addEventListener("click", function () {
            change(item._id, item.name, item.kelas);
        });

        tdAksi.appendChild(buttonDelete);
        tdAksi.appendChild(buttonUbah);

        tr.appendChild(tdNo);
        tr.appendChild(tdId);
        tr.appendChild(tdName);
        tr.appendChild(tdKelas);
        tr.appendChild(tdAksi);

        tbody.appendChild(tr);
    });
}

function token_dong() {
    localStorage.setItem('key', document.getElementById("token_id").value);
    location.reload(true);
}

function change(id, name, kelas) {
    document.getElementById("name_update").value = name;
    document.getElementById("id").value = id;
    document.getElementById("kelas_update").value = kelas;

    formUpdate.style.display = "block";
    formTambah.style.display = "none";
}

function tambah_dong() {
    var name = document.getElementById("name").value;
    var kelas = document.getElementById("kelas").value;

    if (name === "") alert("Nama Wajib Diisi");
    else if (kelas === "") alert("Kelas Wajib Diisi");
    else {
        const data = {
            name: name,
            kelas: kelas
        };

        fetch("https://crudcrud.com/api/" + localStorage.getItem('key') + "/data", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        })
            .then((response) => response.json())
            .then((data) => {
                alert("Data " + name + " Berhasil Disimpan");
                location.reload(true);
            })
            .catch((error) => {
                console.error("Terjadi kesalahan:", error);
            });
    }
}

function deleteData(id, name) {
    fetch("https://crudcrud.com/api/" + localStorage.getItem('key') + "/data/" + id, {
        method: "DELETE",
    })
    .then((response) => {
        alert("Data " + name + " Berhasil Dihapus");
        location.reload(true)
    })
    .catch((error) => {
        console.error("Terjadi kesalahan:", error);
    });
}

function update_dong() {
    var id = document.getElementById("id").value;
    var name = document.getElementById("name_update").value;
    var kelas = document.getElementById("kelas_update").value;

    if (name === "") alert("Nama Wajib Diisi");
    else if (kelas === "") alert("Kelas Wajib Diisi");
    else {
        const data = {
            name: name,
            kelas: kelas
        };

        fetch("https://crudcrud.com/api/" + localStorage.getItem('key') + "/data/" + id, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        })
        .then((response) => {
            alert("Data " + name + " Berhasil Diupdate");
            location.reload(true);
        })
        .then((data) => {})
        .catch((error) => {
            console.error("Terjadi kesalahan:", error);
        });
    }
}